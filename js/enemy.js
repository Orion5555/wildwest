/**
 * Класс врага - дикого бандита
 * @constructor
 */
var Enemy = function () {
    //высота ширина врага
    this.height = 0;
    this.width = [];
    //состояние движения врага
    this.state = {};
    //маштаб вывода на экран
    this.scale = 2.5;
    //смешение спрайка в исходной картинке
    this.sy = 0;
    this.sx = [];
    //положение врага
    this.position = {
        x: 0,
        y: 0
    };
    //кордината Х конечной цели движения
    this.targetX = 0;
    //текущий кадр
    this.curFrame = 0;
    //текущее состояние
    this.curState = "go";
    //текущее смещение внутри состояния
    this.curInc = 0;
    //скорость врага
    this.speed = 10;
    //направление движения
    this.direction = 1;
    //цена за убийство врага
    this.prize = 0;
    //шляпа врага
    this.hat = new Hat();
};


/**
 * Первоначальная инициализация - враг идёт до середины экрана
 */
Enemy.prototype.initFirst = function () {
    this.position.y = window.Util.randomInteger(window.Background.interval[window.Background.index][0] - this.height * this.scale, window.Background.interval[window.Background.index][1] - this.height * this.scale);
    this.targetX = window.Game.width / 2 - this.width[this.state["stand"][0]] * this.scale / 2;
    if (Math.random() > 0.5) {
        this.direction = 1;
        this.position.x = 0;
    } else {
        this.direction = -1;
        this.position.x = window.Game.width;
    }
    this.setState("go");
};

/**
 * Враг идёт обратно, если он победил
 */
Enemy.prototype.goHome = function () {
    this.direction = this.direction * -1;
    if (this.direction == 1) {
        this.targetX = window.Game.width;
    }
    if (this.direction == -1) {
        this.targetX = -this.width[this.state["stand"][0]] * this.scale;
    }
    this.setState("go");
};

/**
 * Отрисовка врага
 */
Enemy.prototype.draw = function () {
    window.Game.ctx.drawImage(window.Enemys.image, this.sx[this.curFrame], this.sy, this.width[this.curFrame], this.height, this.position.x, this.position.y, this.width[this.curFrame] * this.scale, this.height * this.scale);
};

/**
 * Смена состояния движения врага
 * @param state
 */
Enemy.prototype.setState = function (state) {
    this.curState = state;
    this.curInc = 0;
    this.curFrame = this.state[this.curState][this.curInc];
};

/**
 * Получение текущего состояния врага
 * @returns {*|string}
 */
Enemy.prototype.getState = function () {
    return this.curState;
};

/**
 * Движение врага
 */
Enemy.prototype.update = function () {
    this.draw();
    if (this.curState === "go") {
        if (this.direction === 1) {
            if (this.position.x < this.targetX) {
                this.position.x += this.speed;
            } else {
                this.setState("stand");
            }
        } else {
            if (this.position.x > this.targetX) {
                this.position.x -= this.speed;
            } else {
                this.setState("stand");
            }
        }
    }

    var len = this.state[this.curState].length;
    this.curInc += 1;
    if (this.curInc >= len) {
        if (this.curState !== "die") {
            this.curInc = 0;
        } else {
            this.curInc = len - 1;
        }
    }
    this.curFrame = this.state[this.curState][this.curInc];
};