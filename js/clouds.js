/**
 * Все облака - одновременная работа
 * @constructor
 */
var Clouds = function () {
    //массив облаков
    this.arrClouds = [];
    //количество облаков
    this.count = 3;
    //высота и ширина облака
    this.height = 128;
    this.width = 128;

    this.init();
};

/**
 * Загрузка картинок облаков
 */
Clouds.prototype.init = function () {
    this.image = new Image();
    this.image.src = "img/cloud.png";
    this.image.onload = load;

    var self = this;

    function load() {
        for (var i = 0; i < self.count; i++) {
            var cloud = new Cloud();
            cloud.init();
            self.arrClouds.push(cloud);
        }
        window.Preload.loadFlags.clouds = true;
    }
};

/**
 * Отрисовка облаков
 */
Clouds.prototype.update = function () {
    for (var i = 0; i < this.count; i++) {
        cloud = this.arrClouds[i];
        window.Game.ctx.drawImage(this.image, this.width * i, 0, this.width, this.height, cloud.position.x, cloud.position.y, this.width * cloud.scale, this.height * cloud.scale);
        cloud.update();
    }
};
