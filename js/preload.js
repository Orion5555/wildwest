/**
 * Класс прелоадера - используется для ожидания при загрузке картинок
 * @constructor
 */
var Preload = function () {
    //объект прелоадера
    this.preloader = document.getElementById("preloader");
    //флаги загрузки элементов игры
    this.loadFlags = {
        background: false,
        enemys: false,
        clouds: false,
        sounds: false,
        soundImage: false
    };
    this.init();
};

/**
 * Функция постпенного затухания прелоадера
 * @param element
 */
Preload.prototype.fadeOut = function (element) {
    element.style.opacity = 1;

    var interPreloader = setInterval(function () {
        element.style.opacity = element.style.opacity - 0.05;
        if (element.style.opacity <= 0.05) {
            clearInterval(interPreloader);
            window.Preload.preloader.style.display = "none";
            requestAnimFrame(render);
            window.Game.setState("menu");
        }
    }, 16);
};

/**
 * Функция первоначальной инициализации прелоадера
 */
Preload.prototype.init = function () {
    var outerPreloader = setInterval(function () {
        if (window.Preload.loadFlags.background && window.Preload.loadFlags.enemys && window.Preload.loadFlags.clouds && window.Preload.loadFlags.sounds && window.Preload.loadFlags.soundImage) {
            clearInterval(outerPreloader);
            window.Game.setState("initGeneral");
            window.Preload.fadeOut(window.Preload.preloader);
        }
    }, 1000);
};