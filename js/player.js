/**
 * Класс игрока
 * @constructor
 */
var Player = function () {
    this.clearAll();
};

/**
 * Очистка параметров игрока
 */
Player.prototype.clear = function () {
    //время выстрела, сек.
    this.timeShot = 0.0;
    //перменная отключения лишних кликов
    this.flagClick = true;
    //был ли фальстарт
    this.falseSt = false;
};

Player.prototype.clearAll = function () {
    this.timeShot = 0.0;
    //очки
    this.points = 0;
    //число побед
    this.win = 0;
    //число жизней
    this.life = 3;
    this.flagClick = true;
    this.falseSt = false;
    this.startTimePlayer = 0;
};
/**
 * Если пользователь нажал на кнопку мыши
 * @param event
 */
Player.prototype.clickCanvas = function (event) {
    window.Sound.play("fire");
    if (window.Player.flagClick) {
        window.Player.flagClick = false;
        if (window.Player.falseStart()) {
            window.Player.falseSt = true;
            window.Game.setState("defeat");
        } else {
            window.Player.timeShot = +(((new Date().getTime() - window.Player.startTimePlayer) / 1000).toFixed(2));
            var curEnemy = window.Enemys.enemysList[window.Round.curEnemy];
            var flag = window.Player.intersection({
                left: event.offsetX,
                right: event.offsetX,
                top: event.offsetY,
                bottom: event.offsetY
            }, {
                left: curEnemy.position.x,
                right: curEnemy.position.x + curEnemy.width[curEnemy.curFrame] * curEnemy.scale,
                top: curEnemy.position.y,
                bottom: curEnemy.position.y + curEnemy.height * curEnemy.scale
            });
            if ((window.Player.timeShot < window.Round.timeShot) && flag) {
                window.Game.setState("win");
            } else {
                window.Game.setState("defeat");
            }
        }
    }
};

/**
 * Проверка попал ли игрок по врагу
 * @param a
 * @param b
 * @returns {boolean}
 */
Player.prototype.intersection = function (a, b) {
    return (a.left <= b.right &&
    b.left <= a.right &&
    a.top <= b.bottom &&
    b.top <= a.bottom)
};

/**
 * Был ли фальстарт
 * @returns {boolean}
 */
Player.prototype.falseStart = function () {
    return window.Game.curState !== "action";
};