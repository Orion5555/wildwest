/**
 * Вспомогательный класс - для вывода мерцающего сообщения
 * @constructor
 */
var Interval = function () {
    //начало времени отчета
    this.startTime = 0;
    //счетчик используется для мерцания
    this.count = 0;
};

/**
 * Первоначальная инициализация
 */
Interval.prototype.init = function () {
    this.startTime = performance.now();
    this.count = 0;
};

/**
 * Вывод мерчающей надписи
 * @param duration
 * @param callback
 */
Interval.prototype.delayBlink = function (duration, callback) {
    var curTime = performance.now();
    if (curTime - this.startTime < duration) {
        this.count++;
        if (this.count % 2 === 0) {
            if (callback && typeof(callback) === "function") {
                callback();
            }
        }
    }
};