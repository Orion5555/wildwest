/***
 * Класс Заднего фона - Бэкграунда
 * @constructor
 */
var Background = function () {
    //Массив картинок
    this.arrayImages = [];
    //текущий фон
    this.index = 0;
    //интервал движения врага по конкретному фону
    this.interval = [
        [630, 670],
        [720, 768],
        [560, 590]
    ];
    //серая картинка при поражении
    this.grayImg = {};
};

/**
 * Загрузка всех изображений фона в память
 * Инициализация интервала в котором могут перемещаться противники
 * @param obj
 */
Background.prototype.init = function (obj) {
    var loaded = 0;
    var toload = 0;
    var images = [];

    for (var i = 0; i < obj.length; i++) {
        toload++;
        images[i] = new Image();
        images[i].src = "img/" + obj[i];
        images[i].onload = load;
        images[i].onerror = load;
        images[i].onabort = load;
    }

    function load() {
        if (++loaded >= toload) {
            window.Background.arrayImages = images.slice();
            window.Preload.loadFlags.background = true;
        }
    }
};

/**
 * Очистка серого фона игры - используется при поражении
 */
Background.prototype.clearGray = function () {
    this.grayImg = {};
};

/**
 * Отрисовка бэкграунда
 */
Background.prototype.draw = function () {
    if (this.arrayImages.length !== 0) {
        window.Game.ctx.drawImage(this.arrayImages[this.index], 0, 0);
    }
};

/**
 * Отрисовка серого бэкграунда - при поражении
 */
Background.prototype.drawGray = function () {
    if (this.grayImg.length == null) {
        this.img2Gray();
    } else {
        window.Game.ctx.drawImage(this.grayImg, 0, 0);
    }
};

/**
 * Следующий фон - используется при смене раунда
 */
Background.prototype.next = function () {
    if (this.arrayImages.length === 0) return;

    this.index++;

    if (this.index >= this.arrayImages.length) {
        this.index = 0;
    }
};

/**
 * Преобразование фона в черно-белый вариант
 */
Background.prototype.img2Gray = function () {
    window.Game.ctx.drawImage(this.arrayImages[this.index], 0, 0);
    this.grayImg = new Image();
    var imageData = window.Game.ctx.getImageData(0, 0, window.Game.canvas.width, window.Game.canvas.height);
    var data = imageData.data;
    for (var i = 0; i < data.length; i += 4) {
        data[i] = data[i + 1] = data[i + 2] = (data[i] + data[i + 1] + data[i + 2]) / 3;
    }
    window.Game.ctx.putImageData(imageData, 0, 0);
    this.grayImg.src = window.Game.canvas.toDataURL();
};