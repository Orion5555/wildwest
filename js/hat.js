/**
 * Класс шляпа врага, когда враг умирает шляпа падает на землю
 * @constructor
 */
var Hat = function () {
    //шляпа у врага есть?
    this.exist = true;
    //координаты шляпы
    this.position = {
        x: 0,
        y: 0
    };
    //гравитация
    this.gravity = 0.5;
    //ускорение падения
    this.velocity = {
        x: 0,
        y: (Math.random() * 15) - 5
    };
    this.height = 0;
};

/**
 * Первоначальная инициализация шляпы
 */
Hat.prototype.init = function () {
    this.curEnemy = window.Enemys.enemysList[window.Round.curEnemy];
    this.curFrame = this.curEnemy.state.hat[0];
    this.position.x = this.curEnemy.position.x + (this.curEnemy.width[this.curEnemy.state.stand[0]] - this.curEnemy.width[this.curEnemy.state.hat[0]]) * this.curEnemy.scale / 2;
    this.position.y = this.curEnemy.position.y;
    this.velocity = {
        x: 0,
        y: (Math.random() * 15) - 5
    };
    this.gravity = 0.5;
};

/**
 * Отрисовка шляпы
 */
Hat.prototype.draw = function () {
    window.Game.ctx.drawImage(window.Enemys.image, this.curEnemy.sx[this.curFrame], this.curEnemy.sy + this.curEnemy.height - this.height, this.curEnemy.width[this.curFrame], this.height, this.position.x, this.position.y, this.curEnemy.width[this.curFrame] * this.curEnemy.scale, this.height * this.curEnemy.scale);
};

/**
 * Движение шляпы
 */
Hat.prototype.update = function () {
    if (this.exist) {
        this.draw();
        this.velocity.y += this.gravity;
        this.position.y += this.velocity.y;
        if (this.position.y + this.height * this.curEnemy.scale > this.curEnemy.position.y + this.curEnemy.height * this.curEnemy.scale) {
            this.position.y = this.curEnemy.position.y + (this.curEnemy.height - this.height) * this.curEnemy.scale;
            this.velocity.y = 0;
            this.gravity = 0;
        }
    }
};
