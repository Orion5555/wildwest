/**
 * Звуки игры
 * @constructor
 */
var Sound = function () {
    //Массив звуков
    this.arraySound = {};
    //Звуки включены или нет
    this.active = true;
    //Ширина и высота значка звука
    this.width = 20;
    this.height = 20;
    //загрузка звуков
    this.init(["intro.mp3", "die.mp3", "go.mp3", "win.mp3", "fire.wav"]);
};

/**
 * Первоначальная загрузка звуков и значка игры
 * @param obj
 */
Sound.prototype.init = function (obj) {
    var loaded = 0;
    var toload = 0;

    for (var i = 0; i < obj.length; i++) {
        toload++;
        var name = obj[i].split('.')[0];
        var audio = new Audio();
        audio.src = "audio/" + obj[i];
        audio.addEventListener('loadeddata', load);
        this.arraySound[name] = audio;
    }

    function load() {
        if (++loaded >= toload) {
            window.Preload.loadFlags.sounds = true;
        }
    }

    this.image = new Image();
    this.image.src = "img/volume.png";
    this.image.onload = download;

    function download() {
        window.Preload.loadFlags.soundImage = true;
    }
};

/**
 * Остановить все звуки в игре
 */
Sound.prototype.stopAll = function () {
    for (key in this.arraySound) {
        if (this.arraySound.hasOwnProperty(key)) {
            if (!this.arraySound[key].paused) {
                this.arraySound[key].pause();
                this.arraySound[key].currentTime = 0.0;
            }
        }
    }
};

/**
 * Остановить конкретный звук
 * @param key
 */
Sound.prototype.stop = function (key) {
    if (!this.arraySound[key].paused) {
        this.arraySound[key].pause();
        this.arraySound[key].currentTime = 0.0;
    }
};

/**
 * Отрисовка значка звука
 * @param dx
 * @param dy
 */
Sound.prototype.draw = function (dx, dy) {
    var i = this.active ? 0 : 1;
    window.Game.ctx.drawImage(this.image, this.width * i, 0, this.width, this.height, dx, dy, this.width, this.height);
};

/**
 * Проиграть конкретный звук
 * @param track
 */
Sound.prototype.play = function (track) {
    if (this.active) {
        window.Sound.arraySound[track].play();
    }
};