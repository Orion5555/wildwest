/**
 * Класс облаков
 * @constructor
 */
var Cloud = function () {
    //координаты облачка
    this.position = {
        x: 0,
        y: 0
    };
    //направление движения
    this.direction = 1;
    //скорость облачка
    this.speed = 5;
    //маштаб вывода облачка на экран
    this.scale = 0.8;
};

/**
 * Первоначальная инициализация облаков
 */
Cloud.prototype.init = function () {
    this.position.y = window.Util.randomInteger(0, Math.round(window.Game.canvas.height / 3));
    this.speed = window.Util.randomInteger(1, 5);
    this.position.x = window.Util.randomInteger(0, window.Game.canvas.width);
    Math.random() > 0.5 ? this.direction = 1 : this.direction = -1;
};

/**
 * Перемещение облаков
 */
Cloud.prototype.update = function () {
    if (this.direction === 1) {
        if (this.position.x < window.Game.width) {
            this.position.x += this.speed;
        } else {
            this.position.x = 0;
        }
    } else {
        if (this.position.x > 0) {
            this.position.x -= this.speed;
        } else {
            this.position.x = window.Game.width;
        }
    }
};