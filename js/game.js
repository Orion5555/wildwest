/**
 * Конструктор основного файла игры
 * @constructor
 */
var Game = function () {
    //Канвас
    this.canvas = document.getElementById('game');
    //Контекст канваса
    this.ctx = this.canvas.getContext('2d');

    //Ширина, высота канваса
    this.width = this.canvas.clientWidth;
    this.height = this.canvas.clientHeight;
    this.canvas.width = this.width;
    this.canvas.height = this.height;

    //текущая стадия игры
    this.curState = "init";
    this.init();
};

/**
 * Инициализация - создание объектов классов игры
 */
Game.prototype.init = function () {
    window.Util = new Util();
    window.Render = new Render();
    window.Preload = new Preload();
    window.Background = new Background();
    window.Background.init(['bgr01.jpg', 'bgr02.jpg', 'bgr03.jpg']);
    window.Enemys = new Enemys();
    window.Note = new Note();
    window.Clouds = new Clouds();
    window.Sound = new Sound();
    window.Player = new Player();
    window.Local = new Local();
    window.Round = new Round();
};

/**
 * Функция обновления - служит для отрисовки объектов игры в зависимости от стадии игры
 */
Game.prototype.update = function () {
    window.Game.ctx.clearRect(0, 0, Game.width, Game.height);
    if (this.curState === "moveEnemy") {
        window.Round.moveEnemy();
    }
    if (this.curState === "action") {
        window.Round.action();
    }
    if (this.curState === "win") {
        window.Round.win();
    }
    if (this.curState === "defeat") {
        window.Round.defeat();
    }
    if (this.curState === "menu") {
        this.menu();
    }
};

/**
 * Смена стадии игры, в зависимости от смены стадии осуществляется первоначальная инициализация
 * @param state
 */
Game.prototype.setState = function (state) {
    this.curState = state;
    var gunman = window.Enemys.enemysList[window.Round.curEnemy];
    if (state === "menu") {
        this.menuInit();
    }
    if (state === "initRound") {
        window.Sound.stopAll();
        window.Sound.play("go");
        window.Game.canvas.removeEventListener("click", window.Game.clickAny);
        window.Round.init();
    }
    if (state === "moveEnemy") {
        window.Round.interval.init();
    }
    if (state === "action") {
        window.Sound.stopAll();
        window.Round.interval.init();
    }
    if (state === "win") {
        window.Sound.stop("go");
        window.Sound.play("win");
        window.Round.interval.init();
        gunman.hat.init();
        gunman.setState("die");
        window.Player.win++;
        window.Player.points += gunman.prize;
        window.Round.numRound++;
        setTimeout(function () {
            window.Game.setState("initRound")
        }, 5000);
    }
    if (state === "defeat") {
        window.Sound.stop("go");
        window.Sound.play("die");
        window.Round.interval.init();
        gunman.goHome();
        window.Player.life--;
        if (window.Player.life === 0) {
            if (window.Player.points > window.Local.getFieldData()) {
                window.Local.updateField(window.Player.points);
            }
            window.Game.canvas.removeEventListener("click", window.Player.clickCanvas);
            window.Game.canvas.addEventListener("click", window.Game.clickAny);
            setTimeout(function () {
                window.Game.setState("menu");
            }, 10000);
        } else {
            window.Round.numRound++;
            setTimeout(function () {
                window.Game.setState("initRound");
            }, 10000);
        }
    }
};

/*
 Инициализация стадии игры "меню"
 */
Game.prototype.menuInit = function () {
    window.Note.clearSteps();
    window.Background.index = window.Util.randomInteger(0, window.Background.arrayImages.length - 1);
    window.Local.initField();
    window.Game.canvas.removeEventListener("click", window.Player.clickCanvas);
    window.Game.canvas.addEventListener("click", window.Game.clickAny);
    if (window.Player.life === 0) {
        this.Inter = new Interval();
        this.Inter.init();
    }
    window.Sound.stopAll();
    window.Sound.play("intro");
};

/**
 * Отрисовка стадии игры "меню"
 */
Game.prototype.menu = function () {
    window.Background.draw();
    window.Note.TextSmallToBig("Дикий бандит");
    if (window.Player.life === 0) {
        this.Inter.delayBlink(3000, function () {
            window.Note.printTextCenter("Очки: $" + window.Player.points);
        });
    }
    window.Note.printTextBottom(window.Game.width / 2, window.Game.height - 20, "Нажмите любую клавишу мыши...", "#FFF", "center");
    window.Note.printTextTop(window.Game.width / 2, 30, "Ваш лучший результат: $" + window.Local.getFieldData(), "center");
    window.Sound.draw(window.Game.width - 30, window.Game.height - 30);
};

/**
 * Если пользователь кликнул на мышку в меню игры
 * Проверяем, а не хочет ли он выключить звук, если нет переходим к следующей стадии игры
 * @param event
 */
Game.prototype.clickAny = function (event) {
    var soundX = window.Game.width - 30;
    var soundY = window.Game.height - 30;
    var flag = window.Player.intersection({
        left: event.offsetX,
        right: event.offsetX,
        top: event.offsetY,
        bottom: event.offsetY
    }, {
        left: soundX,
        right: soundX + window.Sound.width,
        top: soundY,
        bottom: soundY + window.Sound.height
    });
    if (flag) {
        window.Sound.active = !window.Sound.active;
        if (!window.Sound.active) {
            window.Sound.stop("intro");
        } else {
            window.Sound.play("intro");
        }
    } else {
        window.Player.clearAll();
        window.Round.clear();
        window.Game.setState("initRound");
    }
};
/**
 * Создание объекта Игра
 * @type {Game}
 */
window.Game = new Game();