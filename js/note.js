/**
 * Класс надписи
 * @constructor
 */
var Note = function () {
    //высота надписи
    this.height = 100;
    //смещение по Y
    this.offsetY = 100;
    //скорость затухания надписи
    this.rgbstep = 1;
};

Note.prototype.clearSteps = function () {
    this.step = 60;
    this.steps = 110;
};
/**
 * Вывод сообщения по центру экрана
 * @param text
 */
Note.prototype.printTextCenter = function (text) {
    var ctx = window.Game.ctx;
    this.width = window.Game.width;
    this.offsetX = window.Game.width / 4;
    var grd = ctx.createLinearGradient(this.offsetX, this.offsetY, this.offsetX, this.offsetY + this.height);
    grd.addColorStop(0.000, 'rgba(255, 255, 255, 1.000)');
    grd.addColorStop(1.000, 'rgba(217, 131, 72, 1.000)');
    ctx.lineWidth = 3;
    ctx.strokeStyle = "#AC542C";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.font = "bold 80px Arial";
    ctx.fillStyle = grd;
    ctx.fillText(text, window.Game.canvas.width / 2, this.offsetY + this.height / 2);
    ctx.lineWidth = 2;
    ctx.strokeText(text, window.Game.canvas.width / 2, this.offsetY + this.height / 2);
};

/**
 * Вывод верхней статистики
 * @param dx
 * @param dy
 * @param text
 * @param textA
 */
Note.prototype.printTextTop = function (dx, dy, text, textA) {
    if (typeof textA == 'undefined') {
        textA = "left";
    }
    var ctx = window.Game.ctx;
    ctx.textAlign = textA;
    ctx.textBaseline = "middle";
    ctx.fillStyle = "rgb(172, 84, 44)";
    ctx.font = "bold 35px Arial";
    ctx.fillText(text, dx, dy);
    ctx.strokeStyle = "#fff";
    ctx.lineWidth = 1;
    ctx.strokeText(text, dx, dy);
};

/**
 * Вывод нижней статистики
 * @param dx
 * @param dy
 * @param text
 * @param color
 * @param textA
 */
Note.prototype.printTextBottom = function (dx, dy, text, color, textA) {
    if (typeof color == 'undefined') {
        color = "#FFF";
    }
    if (typeof textA == 'undefined') {
        textA = "left";
    }
    var ctx = window.Game.ctx;
    ctx.textAlign = textA;
    ctx.textBaseline = "middle";
    ctx.fillStyle = color;
    ctx.font = "bold 25px Arial";
    ctx.fillText(text, dx, dy);
};

/**
 * Отрисовка облачка с закругленными краями
 * @param x
 * @param y
 * @param width
 * @param height
 * @param radius
 * @param fill
 * @param stroke
 */
Note.prototype.roundRect = function (x, y, width, height, radius, fill, stroke) {
    var ctx = window.Game.ctx;
    if (typeof stroke == 'undefined') {
        stroke = true;
    }
    if (typeof radius === 'undefined') {
        radius = 5;
    }
    if (typeof radius === 'number') {
        radius = {tl: radius, tr: radius, br: radius, bl: radius};
    } else {
        var defaultRadius = {tl: 0, tr: 0, br: 0, bl: 0};
        for (var side in defaultRadius) {
            if (!defaultRadius.hasOwnProperty(side))
                continue;
            radius[side] = radius[side] || defaultRadius[side];
        }
    }
    ctx.beginPath();
    ctx.moveTo(x + radius.tl, y);
    ctx.lineTo(x + width - radius.tr, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
    ctx.lineTo(x + width, y + height - radius.br);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
    ctx.lineTo(x + radius.bl, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
    ctx.lineTo(x, y + radius.tl);
    ctx.quadraticCurveTo(x, y, x + radius.tl, y);
    ctx.closePath();

    ctx.lineWidth = 5;
    if (stroke) {
        ctx.stroke();
    }
    if (fill) {
        ctx.fill();
    }
};

/**
 * Спецэффект текст из маленького в большой
 * @param text
 * @constructor
 */
Note.prototype.TextSmallToBig = function (text) {
    var ctx = window.Game.ctx;
    var offsetYCenter = ctx.canvas.height / 2 - this.height;
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillStyle = "rgb(172, 84, 44)";
    ctx.strokeStyle = "#fff";
    ctx.font = this.step + "px bold Arial";
    ctx.lineWidth = 3;
    ctx.strokeText(text, ctx.canvas.width / 2, offsetYCenter + this.height / 2);
    ctx.fillText(text, ctx.canvas.width / 2, offsetYCenter + this.height / 2);
    if (this.step < this.steps) {
        this.step++;
    }
};

/**
 * Спецэффект текст постепенно затухает
 * @param text
 * @constructor
 */
Note.prototype.TextFadeUp = function (text) {
    var ctx = window.Game.ctx;
    ctx.font = "80px bold Arial";
    ctx.fillStyle = "rgba(172, 84, 44, " + this.rgbstep + ")";
    ctx.fillText(text, ctx.canvas.width / 2, this.offsetY + this.height / 2);
    if (this.rgbstep < 1) {
        this.rgbstep = this.rgbstep + 0.05;
    }
};