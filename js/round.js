/**
 * Один игровой раунд
 * @constructor
 */
var Round = function () {
    //текущий враг
    this.curEnemy = 0;
    //интервал мерцания надписи
    this.interval = new Interval();
    //время выстрела
    this.timeShot = 0.0;
    //смещение по Y
    this.offsetY = 30;
    //номер раунда
    this.numRound = 0;
};

/**
 * Первоначальная инициализация раунда игры
 */
Round.prototype.init = function () {
    window.Background.next();
    this.curEnemy = window.Util.randomInteger(0, 4);
    window.Enemys.enemysList[this.curEnemy].initFirst();
    window.Game.setState("moveEnemy");
    window.Player.clear();
    window.Game.canvas.addEventListener("click", window.Player.clickCanvas);
    window.Background.clearGray();
    window.Round.timeShot = +(window.Round.timeShot - 0.1).toFixed(2);
    if (window.Round.timeShot <= 0) {
        window.Round.timeShot = window.Util.randomFloat(0.05, 1);
    }
};

/**
 * Очистка при запуске нового раунда
 */
Round.prototype.clear = function () {
    this.timeShot = 1.1;
    this.numRound = 1;
};

/**
 * Стадия игры Перемещение врага
 */
Round.prototype.moveEnemy = function () {
    window.Background.draw();
    var gunman = window.Enemys.enemysList[this.curEnemy];
    gunman.update();
    if (gunman.getState() === "stand") {
        window.Game.setState("action");
        window.Player.startTimePlayer = new Date().getTime();
        gunman.setState("shoot");
    }
    window.Clouds.update();
    this.showStatistics();
    this.interval.delayBlink(2000, function () {
        window.Note.printTextCenter("Раунд " + window.Round.numRound);
    });
};

/**
 * Стадия игры Действие
 */
Round.prototype.action = function () {
    if (+(((new Date().getTime() - window.Player.startTimePlayer) / 1000).toFixed(2)) > window.Round.timeShot) {
        window.Game.setState("defeat");
    }
    window.Background.draw();
    window.Clouds.update();
    this.interval.delayBlink(1000, function () {
        window.Note.printTextCenter("Огонь!");
    });
    var gunman = window.Enemys.enemysList[this.curEnemy];
    gunman.update();
    this.showStatistics("");
};

/**
 * Стадия игры - Победа
 */
Round.prototype.win = function () {
    window.Background.draw();
    window.Clouds.update();
    this.interval.delayBlink(3000, function () {
        window.Note.printTextCenter("Победа!!!");
    });
    var gunman = window.Enemys.enemysList[this.curEnemy];
    gunman.update();
    gunman.hat.update();
    this.showStatistics("win");
};

/**
 * Стадия игры - Поражение
 */
Round.prototype.defeat = function () {
    window.Background.drawGray();
    window.Clouds.update();
    this.interval.delayBlink(3000, function () {
        var text = "Поражение!";
        if (window.Player.falseSt) {
            text = text.concat(" Фальстарт!");
        }
        window.Note.printTextCenter(text);
    });
    var gunman = window.Enemys.enemysList[this.curEnemy];
    gunman.update();
    this.showStatistics("defeat");
};

/***
 * Вывод игровой статистики
 * @param state
 */
Round.prototype.showStatistics = function (state) {
    window.Note.printTextTop(20, this.offsetY, "Время бандита: " + this.timeShot + " сек.");
    window.Note.printTextTop(640, this.offsetY, "Ваше время: " + window.Player.timeShot + " сек.");
    var offsetYBott = window.Game.height - 20;
    window.Note.printTextBottom(20, offsetYBott, "Награда: $" + window.Enemys.enemysList[this.curEnemy].prize);
    if (state === "win") {
        window.Note.printTextBottom(400, offsetYBott, "Очки: $" + window.Player.points, "#0f0");
        window.Note.printTextBottom(650, offsetYBott, "Побед: " + window.Player.win, "#0f0");
    } else {
        window.Note.printTextBottom(400, offsetYBott, "Очки: $" + window.Player.points);
        window.Note.printTextBottom(650, offsetYBott, "Побед: " + window.Player.win);
    }
    if (state === "defeat") {
        window.Note.printTextBottom(890, offsetYBott, "Жизни: " + window.Player.life, "#f00");
    } else {
        window.Note.printTextBottom(890, offsetYBott, "Жизни: " + window.Player.life);
    }
};