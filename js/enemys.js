/**
 * Все враги одним классом
 * @constructor
 */
var Enemys = function () {
    //список врагов
    this.enemysList = [];
    this.init();
};

/**
 * Первоначальния инициализация врагов, загрузка их из картинки
 */
Enemys.prototype.init = function () {
    var heights = [65, 78, 83, 68, 71];
    var widths = [
        [33, 34, 32, 34, 34, 34, 35, 33, 34, 34, 34, 34, 28, 35],
        [33, 34, 34, 30, 26, 26, 25, 30, 30, 32],
        [27, 28, 28, 30, 30, 32, 31, 33, 20, 31],
        [33, 34, 34, 33, 33, 34, 32, 35, 33, 36],
        [33, 34, 34, 31, 31, 32, 31, 34, 34, 22, 35]
    ];
    var states = [
        {
            'go': [3, 4, 5],
            'stand': [13],
            'shoot': [6, 7, 8],
            'die': [9, 10, 11],
            'hat': [12]
        },
        {
            'go': [0, 1, 2],
            'stand': [9],
            'shoot': [3, 4, 5],
            'die': [6, 7, 8],
            'hat': []
        },
        {
            'go': [0, 1, 2],
            'stand': [9],
            'shoot': [3, 4, 5],
            'die': [6, 7],
            'hat': [8]
        },
        {
            'go': [0, 1, 2],
            'stand': [9],
            'shoot': [3, 4, 5],
            'die': [6, 7],
            'hat': [8]
        },
        {
            'go': [0, 1, 2],
            'stand': [10],
            'shoot': [3, 4, 5],
            'die': [6, 7, 8],
            'hat': [9]
        }
    ];
    var prize = [1200, 2300, 3400, 4500, 6700];
    var hatHeights = [9, 0, 8, 15, 14];

    this.image = new Image();
    this.image.src = "img/gunman.png";
    this.image.onload = load;

    var self = this;

    function load() {
        sy = 0;
        for (var i = 0; i < 5; i++) {
            var enemy = new Enemy();
            enemy.height = heights[i];
            enemy.prize = prize[i];
            enemy.width = widths[i].slice();
            enemy.sy = sy;
            sy += heights[i];
            sx = 0;
            for (var j = 0; j < widths[i].length; j++) {
                enemy.sx.push(sx);
                sx += widths[i][j];
            }
            enemy.state = states[i];
            enemy.hat.height = hatHeights[i];
            if (hatHeights[i] === 0) {
                enemy.hat.exist = false;
            }
            self.enemysList.push(enemy);
        }
        window.Preload.loadFlags.enemys = true;
    }
};