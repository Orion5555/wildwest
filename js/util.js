/**
 * Вспомогательный класс утилит
 * @constructor
 */
var Util = function () {
};

/**
 * Случайное цело число
 * @param min
 * @param max
 * @returns {number}
 */
Util.prototype.randomInteger = function (min, max) {
    return Math.round(min - 0.5 + Math.random() * (max - min + 1));
};

/**
 * Случайное дробное число
 * @param min
 * @param max
 * @returns {number}
 */
Util.prototype.randomFloat = function (min, max) {
    return +(Math.random() * (max - min) + min).toFixed(2);
};