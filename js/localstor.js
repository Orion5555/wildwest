/**
 * Класс локального хранилища
 * @constructor
 */
function Local() {
    //локальное хранилище
    this.ls = localStorage;
}

/**
 * Первоначальная инициализация
 */
Local.prototype.initField = function () {
    if (!this.ls['score']) {
        this.clearField('score');
    }
};

/**
 * Обновление поля локального хранилища
 * @param data
 */
Local.prototype.updateField = function (data) {
    this.ls['score'] = +data;
};

/**
 * Получение значения из поля
 * @returns {*}
 */
Local.prototype.getFieldData = function () {
    return this.ls['score'];
};

/**
 * Очистка поля
 */
Local.prototype.clearField = function () {
    this.ls['score'] = 0;
};