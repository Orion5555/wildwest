/**
 * Переменная рендера
 * @type {*|Function}
 */
window.requestAnimFrame = window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    function (callback) {
        window.setTimeout(callback, 1000 / 60);
    };

/**
 * Кдасс рендера
 * @constructor
 */
var Render = function () {
    this.lastUpdateTime = 0;
    this.acDelta = 0;
    this.msPerFrame = 80;
};

/**
 * Функция прорисовки основного цикла игры
 */
function render() {
    requestAnimFrame(render);

    var delta = Date.now() - window.Render.lastUpdateTime;
    if (window.Render.acDelta > window.Render.msPerFrame) {
        window.Render.acDelta = 0;
        window.Game.update();
    } else {
        window.Render.acDelta += delta;
    }
    window.Render.lastUpdateTime = Date.now();
}